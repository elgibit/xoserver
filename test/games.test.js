/**
 * Test cases for game modules.
 *
 * @author elgibit
 */

const expect = require('chai').expect;

// Dependencies
var GamesManager = require('../src/modules/game/games.js');

describe('GamesManager', function() {

  beforeEach(() => {
    this.subject = new GamesManager();
  })

  it('should give us empty collection while asking for a list of games', () => {
    let list = this.subject.list();
    expect(list).to.be.an('object');
    expect(list).to.deep.equal({});
  });

  it('should give us board array', () => {
    let id = this.subject.add(2, 3, 'brad');
    let board = this.subject.get(id).board;
    expect(id).to.be.a('string');
    expect(board).to.be.an('array');
    expect(board.length).to.equal(3);
    expect(board[0].length).to.equal(2);
  });

  it('should give us list of games', () => {
    let id = this.subject.add(4, 4, 'mat');
    let list = this.subject.list();
    expect(id).to.be.a('string');
    expect(list).to.be.an('object');
    expect(list[id]).to.be.an('object');
    expect(list[id].host).to.equal('mat');
  });

});