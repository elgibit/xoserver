/**
 * Test cases for auth modules. Covers auth/user and auth/users modules.
 *
 * @author elgibit
 */

const expect = require('chai').expect;

// Test subject
var UsersManager = require('../src/modules/auth/users.js');

describe('UsersManager', function() {

  beforeEach(() => {
    this.subject = new UsersManager();
  });

  it('should fail register new user when no data passed', () => {
    let uid = this.subject.register();

    expect(uid).to.equal(false);
  });

  it('should register new user named Bobby', () => {
    let uid = this.subject.register('Bobby');
    let data = this.subject.get(uid);

    expect(uid).to.be.an('string');
    expect(data).to.have.a.property('name');
    expect(data).to.have.a.property('session');
    expect(data).to.have.a.property('expire');
    expect(data.name).to.equal('Bobby');
  });

  it ('should fail while asking for a non-existent user', () => {
    let data = this.subject.get('random-invalid-uid');

    expect(data).to.equal(false);
  });

  it('should give us empty collection while asking for a list of users', () => {
    let list = this.subject.list();

    expect(list).to.be.an('object');
    expect(Object.keys(list).length).to.equal(0);
    expect(list).to.deep.equal({});
  });

  it('should give us collection of registered users', () => {
    this.subject.register('Foo');
    this.subject.register('Bar');

    let list = this.subject.list();
    expect(list).to.be.an('object');
    expect(Object.keys(list).length).to.equal(2);
  });

  it('should fail while tryig to remove non-existent user', () => {
    let status1 = this.subject.remove();
    let status2 = this.subject.remove('random-invalid-uid');

    expect(status1).to.equal(false);
    expect(status2).to.equal(false);
  });

  it('should give us ability to remove user', () => {
    let uid = this.subject.register('Foo');
    let status = this.subject.remove(uid);

    expect(status).to.equal(true);
    expect(this.subject.list()).to.deep.equal({});
  });

});