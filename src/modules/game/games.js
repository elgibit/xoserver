// Dependencies
const uuid = require('uuid/v4');
var GameModel = require('./game-model');

/**
 * Games module.
 *
 * @module Games
 * @package Games
 *
 * Works as a service to handle many games.
 */
class GamesManager {

  /**
   * Creates an instance of GamesManager.
   */
  constructor() {
    this.games = {};
  }

  /**
   * Get game instance based on a game id.
   *
   * @public
   * @method get
   * @param  {string} id  Game identifier (GUID)
   * @return {object}     Game instance
   */
  get(id) {
    if (!id || !this.games[id]) {
      return false;
    }

    return this.games[id];
  };

  /**
   * Get list of all games in a clean way. Can be easy passed as a JSON object.
   *
   * @public
   * @method list
   * @return {Object} List of active games
   */
  list() {
    var id, result = {};

    for (id in this.games) {
      if (this.games.hasOwnProperty(id)) {
        result[id] = this.games[id].toRead();
      }
    }

    return result;
  };

  /**
   * Add new game and assig it to host.
   *
   * @public
   * @method add
   * @param  {Integer} width   Board's width
   * @param  {Integer} height  Board's height
   * @param  {String}  host    Host identifier (GUID)
   * @return {String}          Game identifier (GUID)
   */
  add(width, height, host) {
    var id = uuid();

    if (!host) {
      return false;
    }

    this.games[id] = new GameModel(width, height, host);

    return id;
  };

  /**
   * Remove a game.
   *
   * @public
   * @method remove
   * @param  {String} id  Game identifier (GUID)
   * @return {Boolean}    True if deleted successfully
   */
  remove(id) {
    if (!id || !this.users[id]) {
      return false;
    }

    delete this.games[id];
    return true;
  };

}

module.exports = GamesManager;