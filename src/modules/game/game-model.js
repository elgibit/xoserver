/**
 * Game model.
 *
 * @module GameModel
 * @package Board
 *
 * Game structure:
 * - width
 * - height
 * - board
 * - host
 * - guest
 * - visitors
 *
 * While creating a new board, width and height needs to be defined.
 */
class GameModel {

  /**
   * Creates an instance of GameModel.
   *
   * @param {number} width   Number of board columns.
   * @param {number} height  Number of board rows.
   * @param {string} host    Users identifier
   */
  constructor(width, height, host) {
    this.width = width || 10;
    this.height = height || 10;
    this.board = this.create();
    this.host = host;
    this.guest = '';
    this.visitors = [];
  }

  /**
   * Create new board. Get the width and height of a board and create
   * two-dimensional array, each cell at the begining have empty string inside.
   *
   * @method create
   * @return {Array} Returns board as a two-dimensional array
   */
  create() {
    var i, j, result = [];

    if ( ! this.width || ! this.height) {
      return false;
    }

    for (i = 0; i < this.height; i++) {
      result[i] = [];
      for (j = 0; j < this.width; j++) {
        result[i][j] = '';
      }
    }

    return result;
  };

  /**
   * Return a shallow copy of the game's attributes for JSON stringification.
   * This can be used for persistence, serialization, or for augmentation
   * before being sent to the server or client.
   *
   * @method  toRead
   * @return {Object} User with his own properties
   */
  toRead() {
    var result = {
      host: this.host,
      guest: this.guest,
      visitors: this.visitors
    };

    return result;
  };

  /**
   * Mark proper cell inside board with a specific sign, only when cell was
   * previously empty.
   *
   * @method mark
   * @param  {Integer} x    Position X inside board
   * @param  {Integer} y    Position Y inside board
   * @param  {String}  sign Defined sign for current user ('X' or 'O')
   * @return {Boolean}      Retuns true if user properly marked board
   */
  mark(x, y, sign) {
    if (x > this.width || y > this.height || this.board[y][x]) {
      return false;
    }

    // Mark it.
    this.board[y][x] = sign;

    return true;
  };

}

module.exports = GameModel;
