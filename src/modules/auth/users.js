// Dependencies
const uuid = require('uuid/v4');
var User = require('./user-model');

/**
 * Local authentication manager for temporary users.
 *
 * @class UsersManager
 * @package Auth
 *
 * User is logged based on socket session id. All users are stored localy
 * in memory so after server is restarted, all data is wiped.
 */
class UsersManager {

  /**
   * Creates an instance of Users.
   */
  constructor() {

    /**
     * Current users stored here.
     *
     * @type {Object}
     */
    this.users = {};
  }

  /**
   * Get user instance based on user id.
   *
   * @public
   * @method get
   * @param  {String} id  User identifier (UUID)
   * @return {Object}     User instance
   */
  get(id) {
    if (!id || !this.users[id]) {
      return false;
    }

    return this.users[id];
  }

  /**
   * Get list of all logged in users in a clean way.
   *
   * @public
   * @method list
   * @return {Object} List of active users
   */
  list() {
    var id, result = {};

    for (id in this.users) {
      if (this.users.hasOwnProperty(id)) {
        result[id] = this.users[id].toRead();
      }
    }

    return result;
  }

  /**
   * Register new user and log him in.
   *
   * @public
   * @method register
   * @param  {Object} name     New user's name
   * @param  {String} session  Browser session or socket session id
   * @return {Object}          User identifier
   */
  register(name, session) {
    var id = uuid();

    if (!name || name.length < 3) {
      return false;
    }

    // Generate a new user in our collection.
    this.users[id] = new User(name);
    this.users[id].login(session);

    return id;
  }

  /**
   * Remove user from list of registered users
   *
   * @public
   * @method remove
   * @param  {String} id  User identifier (UUID)
   * @return {Boolean}    True if deleted successfully
   */
  remove(id) {
    if (!id || !this.users[id]) {
      return false;
    }

    delete this.users[id];
    return true;
  }

}

module.exports = UsersManager;
