/**
 * User's lifespan after logout - informs manager to
 * remove him from from collection.
 *
 * @constant
 * @type {number}
 */
const LIFESPAN = 360000;

/**
 * User model.
 *
 * @module UserModel
 * @package Auth
 *
 * User structure:
 * - name
 * - session
 * - expire
 *
 * Element name is username, session is socket identifier to recognize user
 * actions and expire is a date set when user disconnects from our server
 * (we need our list clean).
 */
class UserModel {

  /**
   * Creates an instance of UserModel.
   *
   * @param {string} name User name
   */
  constructor(name) {
    this.name = name;
    this.session = '';
    this.expire = 0;
  }

  /**
   * Log user in.
   *
   * @param  {string}  session Browser session or socket session id
   * @return {boolean}         True if user logs in
   */
  login(session) {
    if (!session) {
      return false;
    }

    this.session = session;
    this.expire = 0;

    return true;
  }

  /**
   * Log user out.
   *
   * @method logout
   * @return {boolean} True if user logs out
   */
  logout() {
    var timestamp = new Date().getTime();

    if (!this.session) {
      return false;
    }

    this.session = '';
    this.expire = timestamp + LIFESPAN;

    return true;
  }

  /**
   * Return a shallow copy of the users's attributes for JSON stringification.
   * This can be used for persistence, serialization, or for augmentation
   * before being sent to the server or client.
   *
   * @method toRead
   * @return {Object} User with his own properties
   */
  toRead() {
    var result = {
      name: this.name
    };

    return result;
  };

  /**
   * Check if user is logged in or not.
   *
   * @method isLogges
   * @return {boolean} True if user is loged in
   */
  isLogged() {
    return this.session ? true : false;
  };

}

module.exports = UserModel;
