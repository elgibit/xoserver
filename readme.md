# Noguhts and Crosses online game server

Project xoserver is a server made for Noguhts and Crosses online game. As a
base there is an express server runing, comunication is provided thanks to
socket io.

## Setup

First make sure you have installed node.js >= 4.0.
Clone this repo to your folder.
Hit ```yarn``` to install all dependencies.
Great, that's it! Now you're ready to develop and play!

## Tasks

There are few possible tasks that can help you with futher development:

### Run project:
```text
$ yarn start
```

### Watch for file changes and restart server
```text
$ yarn watch
```

### Run your tests:
```text
$ yarn test
```

### Run your tests (tdd mode):
```text
$ yarn tdd
```
